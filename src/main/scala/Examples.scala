/**
  * Created by mark on 26/03/2017.
  */
object Examples {

  // groupBy
  def groupByFirstChar(strs:List[String])={
    strs.groupBy(str=>str.head)
  }
groupByFirstChar(List("Mark","Mike","Tom"))

  def groupByIsOdd(nums:List[Int])={
    nums.groupBy(num=>num %2==1)
  }
groupByIsOdd(List(1,2,3))
  // sortBy
  def sortByKey(kvs:List[(Int, String)])={
    kvs.sortBy(kv=>kv._1)
  }
sortByKey(List(2->"c",1->"d",4->"a",3->"b"))
  def sum(nums:List[Int])= {
    nums.reduce((acc,x)=>acc+x)
  }
sum(List(40,30,20,10))
  def product(nums:List[Int])= {
    nums.reduce((acc,x)=>acc*x)
  }
  product(List(40,30,20,10))
  def mkString(strs:List[String])= {
    strs.reduce((acc,str)=>acc+" "+str)
  }
  mkString(List("Hello","I","am","Ellen"))




}
