import scala.util.Random

/**Created by mark on 26/03/2017.
  *
  */
object Exercises {
  def sortByValue(kvs: List[(Int, String)]) = {
    kvs.sortBy(kv => kv._2)
  }

  sortByValue(List(2 -> "c", 1 -> "r", 4 -> "o", 3 -> "f"))

  def accumulator(nums: List[Int]) = {
    nums.foldLeft(List(0))((acc, x) => {
      acc :+ acc.last + x
    }).tail
  }

  accumulator(List(1, 2, 3, 4, 5))

  def wordCount(fruits: List[String]) = {
    fruits.groupBy(v => v).mapValues(_.length)
  }

  wordCount(List("Apple", "Banana", "Apple", "Cherry", "Cherry", "Apple"))

  val rnd = Random
  rnd.nextInt()

  def shuffle(nums: List[Int]) = {
    nums.sortBy(i => rnd.nextInt)
  }

  shuffle((1 to 52).toList)
  shuffle((1 to 52).toList).take(10)


}